const rp = require('request-promise');

let getMethod = {
    uri:'http://localhost:8080/jpa/users',
    headers: {
        'User-Agent': 'Request-Promise'
    },
    json: true
}

let deleteMethod = {
    method: 'DELETE',
    uri: 'http://localhost:8080/jpa/users/10001',
    json: true
}

let addMethod = {
    method: 'POST',
    uri: 'http://localhost:8080/jpa/users',
    body: {
        phoneNumber: 10006,
        username: "Teddy2",
        password: "password",
        domain: "teddy.com",
        status: "ACTIVE",
        post: null
    },
    json: true 
}

// Deleting Phone number 10001
rp(deleteMethod).then(() => {
    // Adding Phone number
    rp(addMethod).then(() => {
        // Gettings All data
        rp(getMethod).then((response) => {
            // Get All data
            console.log(response)
        })
    })
})
