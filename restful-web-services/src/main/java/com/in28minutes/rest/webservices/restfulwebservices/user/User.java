package com.in28minutes.rest.webservices.restfulwebservices.user;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

@Entity
public class User {
	
	@Id
	//@GeneratedValue
	private Integer phoneNumber;
	
	//@Size(min=2, message="Name should have atleast 2 characters")
	private String username;
	private String password;
	private String domain;
	private String status;
	
	@OneToOne(mappedBy="user")
	private Post posts;
	
	public User() {
		
	}
	
	public User(Integer phoneNumber, String name, Date birthDate) {
		this.phoneNumber = phoneNumber;
		this.username = name;
	}
	
	public Post getPost() {
		return posts;
	}

	public void setPost(Post post) {
		this.posts = post;
	}
	
	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public Integer getPhoneNumber() {
		return phoneNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String name) {
		this.username = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "User [phoneNumber=" + phoneNumber + ", username=" + username + "]";
	}

}
